import generate
import random
import string
import re
import datetime

intro = '\nYou are on an island inhabited by two types of people, Knights and Knaves.\nKnights always tell the truth, Knaves always lie\nYou have to determine who is a Knight and who is a Knave.\n\n\n'
#number of people in puzle
population=random.randint(2,5)
#default names, should get some more...
names=['Matthew','Mark','Luke','John','James','Peter','Nathanael','Thaddeus','Jude','Philip','Simon','Thomas','Merlin','Galahad','Lancelot','Gawain','Percivale','Lionell','Kay','Tristam','Gareth']


#makes all names rhyme with default name chosen, makes them knights or knaves, 1 is knight, -1 knave
abc=[]
exclude=['A','E','I','O','U','Y','Q','X']
name=random.choice(names)
exclude.append(name[0])
for c in string.ascii_uppercase:
    if c not in exclude:
        abc.append(c)
people=[]
y=[]
people.append([name,random.choice([1,-1])])
y.append([name,random.choice([1,-1])])

for i in range(population-1):
    ab=random.choice(abc)
    abc.remove(ab)
    name=name.replace(name[0],ab)
    
    t=random.choice([1,-1])
    people.append([name,t])
    
    y.append([name,t])

strin=people[0][0]  
n=1
for i in people[1:]:
    
    if n+1 != len(people):
        strin+=', %s'%(i[0])
    else:
        strin+=' and %s'%(i[0])
    n+=1
    
intro+='You are confronted by %i people: %s.\n\n'%(population,strin)

print(intro)

#gets statements, whether true or false for each member of puzzle    
for i in people:
    # xor statements for either x or y, nand statements for neither x nor y
    if i[1]==1:
        print( i[0]+' says:\n')
        if random.choice([True,False])==True:
            statement = generate.True_Statement(population,y)
        else:
            statement = generate.sayTrue(population,y,i)
        if '-1' in statement:
            statement = statement.replace('-1','a knave')
            statement = statement.replace('1','a knight')
        else:
            statement = statement.replace('1','a knight')
        
        if 'Xor' in statement:
            statement = 'Either '+statement.replace('Xor','or')
        elif 'nand' in statement:
            regex = '[A-Za-z]+ is'
            statement=statement.replace(i[0],'I')
            r=re.findall(regex,statement)
            
            for e in r:
                f=e.replace(' is','')
                f='is '+f.strip(' ')
                statement=statement.replace(e,f)
            statement= statement.split('nand')
            #neither batthew is should be neigher is batthew...
            
            s='Neither '+statement[0]+'nor is'+statement[1].replace(' is','')
            statement=s.replace('is I','am I')
            
        statement=statement.replace(i[0],'I').replace('I is','I am')
        if 'would say' in statement:
            s=statement.split(' would say')[0]
            if s!='I':
                a=statement.split('would say')
                statement=a[0]+'would say'+a[1].replace(s,'he')
        
        print( statement+'\n\n')
        
    elif i[1]==-1:
        print( i[0]+' says:\n')
        if random.choice([True,False])==True:
            statement = generate.False_Statement(population,y)
        else:
            statement = generate.sayFalse(population,y,i)
        if '-1' in statement:
            statement = statement.replace('-1','a knave')
            statement = statement.replace('1','a knight')
        else:
            statement = statement.replace('1','a knight')
            
        if 'Xor' in statement:
            statement = 'Either '+statement.replace('Xor','or')
        elif 'nand' in statement:
            regex = '[A-Za-z]+ is'
            statement=statement.replace(i[0],'I')
            r=re.findall(regex,statement)
            
            for e in r:
                f=e.replace(' is','')
                f='is '+f.strip(' ')
                statement=statement.replace(e,f)
            statement= statement.split('nand')
            #neither batthew is should be neigher is batthew...
            
            s='Neither '+statement[0]+'nor is'+statement[1].replace(' is','')
            statement=s.replace('is I','am I')
        statement = statement.replace(i[0],'I').replace('I is','I am')
            
        if 'would say' in statement:
            s=statement.split(' would say')[0]
            if s != 'I':
                a=statement.split('would say')
                statement=a[0]+'would say'+a[1].replace(s,'he')
            
            
        print( statement+'\n\n')
    answername=i[0]
    answertype=i[1]
    if answertype ==1:
        answertype='Knight'
    else:
        answertype='Knave'
    answer='\n%s is a %s\n'%(answername,answertype)
    ##answers.write(answer)
    


