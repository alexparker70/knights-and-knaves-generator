import random
#This is a program which will generate knights and knaves puzzles hopefully. It might also be just another abandoned project
def True_Statement(i,p):
    a=[]
    
    for e in p:
        
        a.append(e)
        
    TrueStatement=[]
    unary =['is','is not']
    operators = [None,'or','and','Xor','nand']
    u =random.choice(unary)
    o = random.choice(operators)
    subject=random.choice(a)
    #a true statement will be  t, not f, t or f, t or t, t and t, not f nor f,t Xor f, not t or t, not t or not f, not f and t, not f and not f, 
    #false statement will be f, not t,f or f, f and f, f and t, not t nor t,f Xor f
    def ts(unary,subject):
        #print unary,subject
        if unary =='is':
            #print unary,subject
            s = subject[0] +' is '+str(subject[1])
        if unary =='is not':
            #print unary,subject
            s=subject[0]+' is not '+str(subject[1]*-1)
        return s
    
    def fs(unary,subject):
        if unary =='is':
            #print unary,subject
            s = subject[0] +' is '+str(subject[1]*-1)
        if unary =='is not':
            #print unary,subject
            s=subject[0]+' is not '+str(subject[1])
        return s
    
    
    #Generates True Statement
    if o:
        #both must be true
        if o=='and':
            choice=random.choice(a)
            a.remove(choice)
            TrueStatement.append(ts(u,choice))
            TrueStatement.append(ts(u,random.choice(a)))
        #at least one must be true
        if o=='or':
            choice=random.choice(a)
            a.remove(choice)
            TrueStatement.append(ts(u,choice))
            for i in range(1):
                if random.choice([True,False])==True:
                    TrueStatement.append(ts(u,random.choice(a)))
                else:
                    TrueStatement.append(fs(u,random.choice(a)))
        #only one can be true
        if o=='Xor':
            choice=random.choice(a)
            a.remove(choice)
            TrueStatement.append(ts(u,choice))
            TrueStatement.append(fs(u,random.choice(a)))
        #both must be false    
        if o=='nand':
            choice=random.choice(a)
            a.remove(choice)
            TrueStatement.append(fs(u,choice))
            TrueStatement.append(fs(u,random.choice(a)))
        
        return TrueStatement[0]+' {} '.format(o)+TrueStatement[1]
    else: 
        
        TrueStatement.append(ts(u,random.choice(a)))
        return TrueStatement[0]
    
    

def False_Statement(i,p):
    a=[]
    for e in p:
        #print i
        a.append(e)
    FalseStatement=[]
    unary =['is','is not']
    operators = [None,'or','and','Xor','nand']
    u =random.choice(unary)
    o = random.choice(operators)
    subject=random.choice(a)
    
    def ts(unary,subject):
        if unary =='is':
            #print unary,subject
            s = subject[0] +' is '+str(subject[1])
            
        if unary =='is not':
            #print unary,subject
            s=subject[0]+' is not '+str(subject[1]*-1)
        return s
    
    def fs(unary,subject):
        if unary =='is':
            #print unary,subject
            s = subject[0] +' is '+str(subject[1]*-1)
            
        if unary =='is not':
            #print unary,subject
            s=subject[0]+' is not '+str(subject[1])
        return s
    
    if o:
        #at least one must be false
        if o=='and':
            choice=random.choice(a)
            
            a.remove(choice)
            
            FalseStatement.append(fs(u,choice))
            for i in range(1):
                if random.choice([True,False])==True:
                    FalseStatement.append(fs(u,random.choice(a)))
                else:
                    FalseStatement.append(ts(u,random.choice(a)))

        #both must be false
        if o=='or':
            choice=random.choice(a)
            
            a.remove(choice)
            
            FalseStatement.append(fs(u,choice))
            FalseStatement.append(fs(u,random.choice(a)))
        #both must be true or both must be false
        if o=='Xor':
            choice=random.choice(a)
            
            a.remove(choice)
            
            if random.choice([True,False])==True:
                FalseStatement.append(fs(u,choice))
                FalseStatement.append(fs(u,random.choice(a)))
            else:
                FalseStatement.append(ts(u,choice))
                FalseStatement.append(ts(u,random.choice(a)))
        #at least one must be true    
        if o=='nand':
            choice=random.choice(a)
            
            a.remove(choice)
            
            FalseStatement.append(ts(u,choice))
            FalseStatement.append(ts(u,random.choice(a)))
            #if random.choice([True,False])==True:
                #FalseStatement.append(fs(u,random.choice(a)))
            #else:
                #FalseStatement.append(ts(u,random.choice(a)))
        return FalseStatement[0]+' {} '.format(o)+FalseStatement[1]
    else: 
        FalseStatement.append(fs(u,random.choice(a)))
        return FalseStatement[0]

#fixed. i think    
def sayTrue(i,p,s):
   
    a=[]
    for i in p:
        a.append(i)
    subject = random.choice(a)
    subsubject=random.choice(a)
    sayer= s
    l=int(subject[1])
    k=int(subsubject[1])
    
    if l==1:
        statement='%s would say that %s is %i'%(subject[0],subsubject[0],k)
    else:
        statement='%s would say that %s is %i'%(subject[0],subsubject[0],k*-1)
    
    return statement
    
#fixed?
def sayFalse(i,p,s):
    a=[]
    for i in p:
        a.append(i)
    subject = random.choice(a)
    subsubject=random.choice(a)
    sayer= s
    
    l=int(subject[1])
    k=int(subsubject[1])
    if l==1:
        statement='%s would say that %s is %i'%(subject[0],subsubject[0],k*-1)
    else:
        statement='%s would say that %s is %i'%(subject[0],subsubject[0],k)
    return statement

